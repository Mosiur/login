<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../login&registration/style.css">
</head>
<body>
    <div class="container">
        <div class="login_container">
        	<div><?php require 'test.php';?></div>
            <h2>login</h2>
            <form class="registration_form" action="login.php" method="POST">
                    <label>Username:</label>
                    <input type="text" name="username" placeholder="Enter User Name" required="required" />

                    <label>Password:</label>
                    <input type="Password" name="userpass" placeholder="Enter Password" required="required" />

                    <input type="Submit" name="login" value="login">
                    <a href="reg.php"><button type="button" class="signupbtn">signup</button></a>
                    <span class="psw">Forgot <a href="login.php">password?</a></span>

            </form>
        </div>
    </div>
</body>
</html>
